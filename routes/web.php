<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::group(['prefix' => 'kiosk'], function () {
    Route::GET('/', 'KioskController@index');
    Route::POST('/store', 'KioskController@store');
    // Route::GET('/update', 'LoginController@update')->middleware('authz');
});

// Route::get('/kiosk', 'KioskController@index');