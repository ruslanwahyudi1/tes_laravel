@extends('component.layout')

@section('content')
<div class="container-fluid">
    <section class="page--header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <h2 class="page--title h5">admin</h2>
            <ul class="breadcrumb">
              <li class="breadcrumb-item active"><span>Data Admin</span></li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <div class="row gutter-20 mt-2">
      <div class="col-md-12">
        <div class="loading" align="center" style="display: none;">
          <img src="{{ asset('template/') }}/assets/img/loading.gif" width="60%">
        </div>

        <div class="panel main-layer">
          <div class="panel-content panel-main">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12 form-inline main-layer p-l-10 m-b-10">
                <button type="button" class="btn btn-sm btn-rounded btn-primary btn-add">
                  <i class="fa fa-plus"></i> &nbsp Tambah Admin
                </button>
              </div>

              <div class="col-md-8 col-sm-8 col-xs-12 main-layer panelSearch m-b-10">
                <div class="form-inline pos-absolute right-10">
                  <div class="form-group">
                    <select class="form-control-sm form-control input-s-sm inline v-middle option-search" id="search-option">
                    </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control-sm form-control" placeholder="Search" id="search">
                  </div>
                </div>
              </div>
              <div class='clearfix'></div>

              <div class="col-md-12 p-0">
                <div class="table-responsive">
                  <table style="min-height: 200px;" class="table table-striped b-t b-light" id="datagrid"><thead><tr id="thead-title"><th sortable="false" style="text-align: center; width: 20px;">No</th><th title="name" sortable="true" style="text-align: left; width: 100px; cursor: pointer;">Nama<div class="span_wrapper" style="position: relative; display: inline;"><span style="width: 0px; height: 0px; border-width: 4px 4px 5px; border-style: solid; border-color: transparent transparent rgb(204, 204, 204); border-image: initial; position: absolute; margin-left: 5px; margin-top: 0px;"></span><span style="width: 0px; height: 0px; border-width: 5px 4px 4px; border-style: solid; border-color: rgb(204, 204, 204) transparent transparent; border-image: initial; position: absolute; margin-left: 5px; margin-top: 11px;"></span></div></th><th title="username" sortable="true" style="text-align: left; width: 100px; cursor: pointer;">Username<div class="span_wrapper" style="position: relative; display: inline;"><span style="width: 0px; height: 0px; border-width: 4px 4px 5px; border-style: solid; border-color: transparent transparent rgb(204, 204, 204); border-image: initial; position: absolute; margin-left: 5px; margin-top: 0px;"></span><span style="width: 0px; height: 0px; border-width: 5px 4px 4px; border-style: solid; border-color: rgb(204, 204, 204) transparent transparent; border-image: initial; position: absolute; margin-left: 5px; margin-top: 11px;"></span></div></th><th title="email" sortable="true" style="text-align: left; width: 100px; cursor: pointer;">Email<div class="span_wrapper" style="position: relative; display: inline;"><span style="width: 0px; height: 0px; border-width: 4px 4px 5px; border-style: solid; border-color: transparent transparent rgb(204, 204, 204); border-image: initial; position: absolute; margin-left: 5px; margin-top: 0px;"></span><span style="width: 0px; height: 0px; border-width: 5px 4px 4px; border-style: solid; border-color: rgb(204, 204, 204) transparent transparent; border-image: initial; position: absolute; margin-left: 5px; margin-top: 11px;"></span></div></th><th title="menu" sortable="false" style="text-align: center; width: 50px;">Menu</th><th title="isBanned" sortable="true" style="text-align: center; width: 100px; cursor: pointer;">Status<div class="span_wrapper" style="position: relative; display: inline;"><span style="width: 0px; height: 0px; border-width: 4px 4px 5px; border-style: solid; border-color: transparent transparent rgb(204, 204, 204); border-image: initial; position: absolute; margin-left: 5px; margin-top: 0px;"></span><span style="width: 0px; height: 0px; border-width: 5px 4px 4px; border-style: solid; border-color: rgb(204, 204, 204) transparent transparent; border-image: initial; position: absolute; margin-left: 5px; margin-top: 11px;"></span></div></th></tr></thead><tbody><tr class="main-row"><td style="text-align: center;">1</td><td style="text-align: left; width: 100px;" class="undefined">Admin</td><td style="text-align: left; width: 100px;" class="undefined">admin</td><td style="text-align: left; width: 100px;" class="undefined">admin@gmail.com</td><td style="text-align: center; width: 50px;" class="undefined"><div class="btn-group"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a><ul class="dropdown-menu pull-right p-5"><li onclick="detail(0)"><a href="javascript:void(0);" class="btn btn-warning btn-rounded btn-sm w-100 mb-1"><i class="fa fa-eye"></i> Detail</a></li><li onclick="nonActive(0)"><a href="javascript:void(0);" class="btn btn-info btn-rounded btn-sm w-100 mb-1"><i class="fa fa-lock"></i> Nonaktifkan</a></li><li onclick="updated(0)"><a href="javascript:void(0);" class="btn btn-primary btn-rounded btn-sm w-100 mb-1"><i class="fa fa-edit"></i> Edit</a></li><li onclick="deleted(0)"><a href="javascript:void(0);" class="btn btn-danger btn-rounded btn-sm w-100"><i class="fa fa-trash"></i> Hapus</a></li></ul></div></td><td style="text-align: center; width: 100px;" class="undefined"><span class="label bg-green"><i class="fa fa-unlock"></i> Aktif</span></td></tr></tbody></table>
              </div>
                <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-2 hidden-xs">
                      <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                    </div>
                    <div class="col-sm-5 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                    </div>
                    <div class="col-sm-5 text-right text-center-xs">
                      <ul class="pagination pagination-sm justify-content-center float-r m-t-0 m-b-0" id="paging"></ul>
                    </div>
                  </div>
                </footer>
              </div>
              <div class='clearfix'></div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@endsection
