<aside class="sidebar" data-trigger="scrollbar" style="z-index: 2">
    <div class="sidebar--nav">
      <ul>
        <li>
          <ul>
            <li class='{{ (request()->is('/')) ? 'active' : '' }}'>
              <a href="">
                <i class="fa fa-home"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
              <a href="">
                <i class="fa fa-users"></i>
                <span>Data Pengguna</span>
              </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-list"></i>
                  <span>Data Poli</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-archive"></i>
                  <span>Data Pelayanan</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-address-book"></i>
                  <span>Data Pasien</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-clone"></i>
                  <span>Data Dokter</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-briefcase"></i>
                  <span>Data ICD X</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-copy"></i>
                  <span>Data Penyakit</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-book"></i>
                  <span>Laporan Kas Masuk</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-book"></i>
                  <span>Laporan Rawat Jalan</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-book"></i>
                  <span>Laporan Rawat Inap</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-book"></i>
                  <span>Laporan Farmasi</span>
                </a>
            </li>
            <li class='{{ (request()->is('/s')) ? 'active' : '' }}'>
                <a href="">
                  <i class="fa fa-sign-out-alt"></i>
                  <span>Logout</span>
                </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
</aside>
