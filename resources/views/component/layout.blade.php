<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">
<!-- Mirrored from themelooks.net/demo/dadmin/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Oct 2018 09:17:54 GMT -->
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>RS BAHKTI RAHAYU</title>
  <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" type="image/png" href=""/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CMontserrat:400,500">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/jquery-ui.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/perfect-scrollbar.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/horizontal-timeline.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/style.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/animate.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/util.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/custom.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/plugins/sweetalert/sweetalert.css">
  <script src="{{ asset('template/') }}/plugins/sweetalert/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="{{ asset('template/') }}/css/bootstrap-datetimepicker.min.css"/>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/chosen/chosen.css"/>

  <style media="screen">
    .btn-ket{
      font-size:11px !important;
      height:24px !important;
      padding:1px 10px !important;
    }
    .panel, .label-text{ color: #000; }

    .navbar--btn { padding: 10px 0 10px 20px !important; }
    .navbar--nav .nav-link { padding: 10px 15px !important; }
    .navbar--nav .nav-link .badge { top: 13px !important; }
    .navbar--header .logo { padding: 10px !important; }
    .sidebar { top: 70px !important; }
    .main--container { padding-top : 70px !important; }
  </style>
</head>
<body>
  <div class="wrapper">

    @include('component.header')

    @include('component.sidebar')

    <main class="main--container">

        @yield('content')

        @include('component.footer')
    </main>
  </div>

  <script src="{{ asset('template/') }}/assets/js/jquery.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/jquery-ui.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/jquery.maskMoney.js"></script>
  <script src="{{ asset('template/') }}/assets/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/perfect-scrollbar.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/jquery.sparkline.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/raphael.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/horizontal-timeline.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/main.js"></script>
  <script src="{{ asset('template/') }}/js/animate.js"></script>
  <script src="{{ asset('template/') }}/js/validate.js"></script>
  <script src="{{ asset('template/') }}/js/datagrid.js"></script>
  <script src="{{ asset('template/') }}/js/bootstrap-datetimepicker.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="{{ asset('template/') }}/js/chosen.jquery.min.js"></script>
  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  </script>

</body>
<!-- Mirrored from themelooks.net/demo/dadmin/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Oct 2018 09:17:54 GMT -->
</html>
