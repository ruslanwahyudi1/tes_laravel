<header class="navbar navbar-fixed" style="background-color: #235f13" style="z-index: -1" >
    <div class="navbar--header">
      <a href="" class="logo text-center d-flex justify-content-center align-items-center text-white"></h4>
        <h5>RS BAHKTI RAHAYU</h5>
      </a>
      <a href="javascript:void(0)" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar" style="color:#fff;">
        <i class="fa fa-bars"></i>
      </a>
    </div>
    <a href="javascript:void(0)" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar" style="color:#fff;">
      <i class="fa fa-bars"></i>
    </a>
    <div class="navbar--nav ml-auto">
      <ul class="nav">
        <li class="nav-item dropdown nav--user">
          <a href="javascript:void(0)" class="nav-link text-white" data-toggle="dropdown">
            <img src="{{ asset('template/') }}/assets/img/avatars/user_girl.png" alt="" class="rounded-circle">
            <span>ADMIN</span>
            <i class="fa fa-angle-down"></i>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="">
                <i class="far fa-user"></i>Profile User
              </a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
              <a href="">
                <i class="fa fa-power-off"></i>Logout
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </header>
