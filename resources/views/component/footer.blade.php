<footer class="main--footer main--footer-light p-t-15 p-b-15">
    <p>Copyright &copy; 2020 Development by <a href="http://natusi.co.id" target="_blank"><i>CV. Natusi</i></a>. <span class="float-r"><b>Version</b> 1.1.0</span></p>
</footer>
