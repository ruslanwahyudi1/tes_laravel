<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">
<!-- Mirrored from themelooks.net/demo/dadmin/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Oct 2018 09:17:54 GMT -->
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>RS BAHKTI RAHAYU</title>
  <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" type="image/png" href=""/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CMontserrat:400,500">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/jquery-ui.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/select2.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/perfect-scrollbar.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/horizontal-timeline.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/assets/css/style.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/animate.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/util.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/custom.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/plugins/sweetalert/sweetalert.css">
  <script src="{{ asset('template/') }}/plugins/sweetalert/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="{{ asset('template/') }}/css/bootstrap-datetimepicker.min.css"/>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <link rel="stylesheet" href="{{ asset('template/') }}/css/chosen/chosen.css"/>

  <style media="screen">
    .btn-ket{
      font-size:11px !important;
      height:24px !important;
      padding:1px 10px !important;
    }
    .panel, .label-text{ color: #000; }

    .navbar--btn { padding: 10px 0 10px 20px !important; }
    .navbar--nav .nav-link { padding: 10px 15px !important; }
    .navbar--nav .nav-link .badge { top: 13px !important; }
    .navbar--header .logo { padding: 10px !important; }
    .sidebar { top: 70px !important; }
    .main--container { padding-top : 70px !important; }

    * {
    margin: 0;
    padding: 0
}

body {
    height : 10px;
    overflow-x: hidden;
    background: #000000
}

#bg-div {
    height : 0px;
    margin: 0;
}

#border-btm {
    padding-bottom: 20px;
    margin-bottom: 0px;
    box-shadow: 0px 35px 2px -35px lightgray
}

#test {
    margin-top: 0px;
    margin-bottom: 40px;
    border: 1px solid #FFE082;
    border-radius: 0.25rem;
    width: 60px;
    height: 10px;
    background-color: #FFECB3
}

.active1 {
    color: #00C853 !important;
    font-weight: bold
}

.bar4 {
    width: 35px;
    height: 5px;
    background-color: #ffffff;
    margin: 6px 0
}

.list-group .tabs {
    color: #000000
}

#menu-toggle {
    height: 50px
}

#new-label {
    padding: 2px;
    font-size: 10px;
    font-weight: bold;
    background-color: red;
    color: #ffffff;
    border-radius: 5px;
    margin-left: 5px
}

#sidebar-wrapper {
    min-height: 100vh;
    margin-left: -15rem;
    -webkit-transition: margin .25s ease-out;
    -moz-transition: margin .25s ease-out;
    -o-transition: margin .25s ease-out;
    transition: margin .25s ease-out
}

#sidebar-wrapper .sidebar-heading {
    padding: 0.875rem 1.25rem;
    font-size: 1.2rem
}

#sidebar-wrapper .list-group {
    width: 15rem
}

#page-content-wrapper {
    min-width: 100vw;
    padding-left: 20px;
    padding-right: 20px
}

#wrapper.toggled #sidebar-wrapper {
    margin-left: 0
}

.list-group-item.active {
    z-index: 2;
    color: #fff;
    background-color: #fff !important;
    border-color: #fff !important
}

@media (min-width: 768px) {
    #sidebar-wrapper {
        margin-left: 0
    }

    #page-content-wrapper {
        min-width: 0;
        width: 100%
    }

    #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
        display: none
    }
}

.card0 {
    margin-top: 10px;
    margin-bottom: 10px;
    /* height : 100px; */
}

.top-highlight {
    color: #00C853;
    font-weight: bold;
    font-size: 20px
}

.form-card input,
.form-card textarea {
    padding: 10px 15px 5px 15px;
    border: none;
    border: 1px solid lightgrey;
    border-radius: 6px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: arial;
    color: #2C3E50;
    font-size: 14px;
    letter-spacing: 1px
}

.form-card input:focus,
.form-card textarea:focus {
    -moz-box-shadow: 0px 0px 0px 1.5px skyblue !important;
    -webkit-box-shadow: 0px 0px 0px 1.5px skyblue !important;
    box-shadow: 0px 0px 0px 1.5px skyblue !important;
    font-weight: bold;
    border: 1px solid skyblue;
    outline-width: 0
}

input.btn-success {
    height: 50px;
    color: #ffffff;
    opacity: 0.9
}

#below-btn a {
    font-weight: bold;
    color: #000000
}

.input-group {
    position: relative;
    width: 100%;
    overflow: hidden
}

.input-group input {
    position: relative;
    height: 90px;
    margin-left: 1px;
    margin-right: 1px;
    border-radius: 6px;
    padding-top: 30px;
    padding-left: 25px
}

.input-group select {
    position: relative;
    height: 90px;
    width : 100%;
    margin-left: 1px;
    margin-right: 1px;
    border-radius: 6px;
    padding-top: 30px;
    padding-left: 25px;
    margin-bottom:30px;
}

.input-group label {
    position: absolute;
    height: 24px;
    background: none;
    border-radius: 6px;
    line-height: 48px;
    font-size: 15px;
    color: gray;
    width: 100%;
    font-weight: 100;
    padding-left: 25px
}

input:focus+label {
    color: #1E88E5
}

#qr {
    margin-bottom: 150px;
    margin-top: 50px
}

  </style>
</head>
<body>
  
<div class="container-fluid px-0" id="bg-div">
    <div class="row justify-content-center">
        <div class="col-lg-9 col-12">
            <div class="card card0">
                <div class="d-flex" id="wrapper">
                    <!-- Sidebar -->
                    <div class="bg-light border-right" id="sidebar-wrapper">
                        <div class="sidebar-heading pt-5 pb-4"><strong>RS BHAKTI RAHAYU</strong></div>
                        <div class="list-group list-group-flush"> 
                            <a data-toggle="tab" href="#menu2" id="tab2" class="tabs list-group-item active1">
                                <div class="list-div my-2">
                                    <div class="fa fa-credit-card"></div> &nbsp;&nbsp; AMBIL ANTRIAN
                                </div>
                            </a>  

                            <a data-toggle="tab" href="#menu1" id="tab1" class="tabs list-group-item bg-light">
                                <div class="list-div my-2">
                                    <div class="fa fa-calendar-check"></div> &nbsp;&nbsp; KONFIRMASI
                                </div>
                            </a> 
                            
                        </div>
                    </div> <!-- Page Content -->
                    <div id="page-content-wrapper">
                        <div class="row pt-3" id="border-btm">
                            <!-- <div class="col-4"> <button class="btn btn-success mt-4 ml-3 mb-3" id="menu-toggle">
                                    <div class="bar4"></div>
                                    <div class="bar4"></div>
                                    <div class="bar4"></div>
                                </button> </div> -->
                            <div class="col-8">
                                <div class="row justify-content-right">
                                    <div class="col-12">
                                        <p class="mb-0 mr-4 mt-4 text-right">{{$time}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-content">
                            <div id="menu1" class="tab-pane">
                                <div class="row justify-content-center">
                                    <div class="col-11">
                                        <div class="form-card">
                                            <!-- <h3 class="mt-0 mb-4 text-center">Enter bank details to pay</h3> -->
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="input-group"> <input type="text" id="bk_nm" placeholder="Masukkan No. Antrian"> <label>NO ANTRIAN</label> </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-12"> <input type="submit" value="SIMPAN" class="btn btn-success placeicon"> </div>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="menu2" class="tab-pane in active">
                                <div class="row justify-content-center">
                                    <div class="col-11">
                                        <div class="form-card">
                                            <!-- <h3 class="mt-0 mb-4 text-center">Enter your card details to pay</h3> -->
                                            <form class="form" name="formambilantri" id="formambilantri" enctype="multipart/formdata" method="GET">
                                                {!! csrf_field() !!}
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="input-group"> 
                                                            <!-- <input type="text" name="exp" id="exp" placeholder="MM/YY" minlength="5" maxlength="5">  -->
                                                            <select name="layanan" class="form-select" aria-label="Default select example">
                                                                @foreach($loket as $key)
                                                                    <option value=''>{{$key['name_group']}}</option>
                                                                @endforeach
                                                                
                                                            </select>
                                                            <label>LAYANAN</label> 
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="input-group"> 
                                                            <input type="text" name="no_antrian" placeholder="&#9679;&#9679;&#9679;" class="placeicon" minlength="3" maxlength="3"> 
                                                            <select name="tujuan" class="form-select" aria-label="Default select example">
                                                            
                                                                @foreach($poli as $key)
                                                                    <option value=''>{{$key['name_group']}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label>POLI</label> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12"> <input type="submit" value="SIMPAN" class="btn btn-success placeicon"> </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="menu3" class="tab-pane">
                                <div class="row justify-content-center">
                                    <div class="col-11">
                                        <h3 class="mt-0 mb-4 text-center">Scan the QR code to pay</h3>
                                        <div class="row justify-content-center">
                                            <div id="qr"> <img src="https://i.imgur.com/DD4Npfw.jpg" width="200px" height="200px"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  <script src="{{ asset('template/') }}/assets/js/jquery.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/jquery-ui.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/jquery.maskMoney.js"></script>
  <script src="{{ asset('template/') }}/assets/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/perfect-scrollbar.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/jquery.sparkline.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/raphael.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/horizontal-timeline.min.js"></script>
  <script src="{{ asset('template/') }}/assets/js/main.js"></script>
  <script src="{{ asset('template/') }}/js/animate.js"></script>
  <script src="{{ asset('template/') }}/js/validate.js"></script>
  <script src="{{ asset('template/') }}/js/datagrid.js"></script>
  <script src="{{ asset('template/') }}/js/bootstrap-datetimepicker.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="{{ asset('template/') }}/js/chosen.jquery.min.js"></script>
  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  </script>

    <script>
        $(document).ready(function(){
        //Menu Toggle Script
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        });

        // For highlighting activated tabs
        $("#tab1").click(function () {
        $(".tabs").removeClass("active1");
        $(".tabs").addClass("bg-light");
        $("#tab1").addClass("active1");
        $("#tab1").removeClass("bg-light");
        });
        $("#tab2").click(function () {
        $(".tabs").removeClass("active1");
        $(".tabs").addClass("bg-light");
        $("#tab2").addClass("active1");
        $("#tab2").removeClass("bg-light");
        });
        $("#tab3").click(function () {
        $(".tabs").removeClass("active1");
        $(".tabs").addClass("bg-light");
        $("#tab3").addClass("active1");
        $("#tab3").removeClass("bg-light");
        });

        $('#formambilantri').submit(function(event) {
            // alert("jekjek");
            event.preventDefault();
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: './store', // the url where we want to POST
                data: $('#formambilantri').serializeArray(), // our data object
                dataType: 'json', // what type of data do we expect back from the server
                beforeSend:function(){
                    // $(".formlabel").addClass("sr-only");
                    // $('#savemenus').attr('disabled',true).html("<i class='fa fa-spinner fa-spin'></i> processing");
                }
            }).done(function(data) {
                $('.name').text(''); // Label Error name Role
                showtoastr(data.status_txt, data.message, data.status_txt);
                tables.ajax.reload();
                $('#formambilantri')[0].reset();
            }).fail(function(data) {
                // var datas = JSON.parse(data.re);
                $.each(data.responseJSON.message,function(index,value){
                    showtoastr('error', value, index );
                    $('.label'+index).removeClass("sr-only");
                    $('.label'+index).text(value);
                });
            }).always(function() {
                $('#savemenus').attr('disabled',false).html("<i class='fa fa-save'></i> Save");
                tables.ajax.reload();
            });
        });

        })
    </script>
</body>
<!-- Mirrored from themelooks.net/demo/dadmin/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Oct 2018 09:17:54 GMT -->
</html>
