<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
// use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function convertTglIndonesian($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
            );
            $pecahkan = explode('-', $tanggal);
            
            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun
            
            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
    public function getCurrentTime(){
        return $this->convertTglIndonesian(date("Y-m-d"))." ".date("H:i:s");
    }

    public function validators($datas, $roles,$messages)
    {
        $validator = Validator::make($datas,$roles,$messages);

        if ($validator->fails()) {
             $response = [
                'success' => false,
                'status' => 'fails',
                'message' => trans("messages.validate-check-fail"),
                'messages' => $validator->errors(),
            ];
            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);  
            $return->throwResponse();
        }
    }
}
