<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Groupuser;
use App\Models\Kiosk;
use Illuminate\Support\Facades\DB;

class KioskController extends Controller
{
    //
    public function index (Request $request)
    {
    	$data['time']           = $this->getCurrentTime();
        $data['loket']          = Groupuser::where('type_group', 'loket')->get();
        $data['poli']           = Groupuser::where('type_group', 'poli')->get();
        return view('kiosk.index', $data);
    }

    public function store(Request $request)
    {
        
        $attributes = $request->only(['no_antrian']);
        $roles      = [
            'no_antrian'       => "required",
        ];
        $messages = [
            'required' => "Wajib diisi"
        ];
        // $this->validators($attributes, $roles, $messages);

        // $actions = Actions::all();

        DB::beginTransaction();
        try {
            $data = Kiosk::create($attributes);

            DB::commit();
            $response = responseSuccess(trans("messages.create-success"), $data);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (Exception $e) {
            DB::rollback();
            $response           = responseFail(trans("messages.create-fail"),$e->getMessage());
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);

        }
    }
}
