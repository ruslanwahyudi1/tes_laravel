<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kiosk extends Model
{
    //
    protected $table        = "kiosk_antrian";
    protected $primaryKey   = "id";
    protected $fillable = ['id', 'no_antrian', 'status', 'created_at', 'updated_at'];

}
